import argparse
# import os
import subprocess
import shlex

parser = argparse.ArgumentParser(description = 'Runs replicates of the same job in parallel.')
parser.add_argument('command', help = 'The command to run in parallel.')
parser.add_argument('-n','--numreps',type=int,help = 'Number of replicates to run of the same process.  Default: number processers.')
parser.add_argument('-p','--passrank',action='store_true',help = 'Flag to pass the rank of the process on to the subprocess.')
args = parser.parse_args()


from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
nprocs = comm.Get_size()
nreps = nprocs
if args.numreps is not None:
    nreps = args.numreps

for i in xrange(rank,nreps,nprocs):
    if args.passrank:
        subprocess.call(shlex.split(args.command)+[str(i)],shell=False)
    else:
        subprocess.call(shlex.split(args.command),shell=False)

